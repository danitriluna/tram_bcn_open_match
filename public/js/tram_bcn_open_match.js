var actualTime = 0;
var send
var intervalTimer;
var playerTake;
var valuesPoints = ["0", "15", "30", "40" , "AV", ""];
var actualSet = 1;
var playsUser1 = 0;
var playsUser2 = 0;
var finish = false;
var activeTieBreak = false;

function startTime(){
    intervalTimer = setInterval(function(){
        actualTime++;
        // console.log(actualTime);
        if(actualTime % 10 == 0){
            sendTime();
        }
        updateTime();
    }, 1000);
}

function stopTime(){
    clearInterval(intervalTimer);
}

function sendTime(){
    $.ajax({
        url: $("body").attr('data-baseurl')+"/match/updateTime",
        data: {
            time: actualTime
        },
        type: 'POST'
    });
}

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h  : "00";
    var mDisplay = m > 0 ? m  : "00";
    return hDisplay.toString().padStart(2, "0") + ":" + mDisplay.toString().padStart(2, "0"); 
}

function updateTime(){
    $(".content-time-match .time").html(secondsToHms(actualTime));
}

function getTime(){
    var time = $(".content-time-match .time").html().split(":");
    console.log("time", time);
    return parseInt((time[0]*60*60) + (time[1]*60));
}

function changeStatusButtonsMatch(){
    if($(".button-finish").hasClass("disabled")){
        $(".button-finish").removeClass("disabled");
        $(".button-suspend").removeClass("disabled");
    }else{
        $(".button-finish").addClass("disabled");
        $(".button-suspend").addClass("disabled");
    }
}

function changeStatusPlayer1(){
    if($(".content-actions-game .button-player-ace.player1").hasClass("disabled")){
        $(".content-actions-game .button-player.player1 .saca").addClass("active");
        $(".result-game-info .points-player-1 .saca-player-1").addClass("active");
        $(".content-results .player1 .ball").addClass("active");

        $(".content-actions-game .button-player-ace.player1").removeClass("disabled");
        $(".content-actions-game .button-player-fault.player1").removeClass("disabled");
    }else{
        $(".content-actions-game .button-player.player1 .saca").removeClass("active");
        $(".result-game-info .points-player-1 .saca-player-1").removeClass("active");
        $(".content-results .player1 .ball").removeClass("active");

        // $(".content-actions-game .button-player.player1").addClass("disabled");
        $(".content-actions-game .button-player-ace.player1").addClass("disabled");
        $(".content-actions-game .button-player-fault.player1").addClass("disabled");
    }

    $(".content-actions-game .button-player.player1").removeClass("disabled");
    $(".content-actions-game .button-player.player2").removeClass("disabled");
}

function changeStatusPlayer2(){
    if($(".content-actions-game .button-player-ace.player2").hasClass("disabled")){
        $(".content-actions-game .button-player.player2 .saca").addClass("active");
        $(".result-game-info .points-player-2 .saca-player-2").addClass("active");
        $(".content-results .player2 .ball").addClass("active");

        $(".content-actions-game .button-player-ace.player2").removeClass("disabled");
        $(".content-actions-game .button-player-fault.player2").removeClass("disabled");
    }else{
        $(".content-actions-game .button-player.player2 .saca").removeClass("active");
        $(".result-game-info .points-player-2 .saca-player-2").removeClass("active");
        $(".content-results .player2 .ball").removeClass("active");

        // $(".content-actions-game .button-player.player2").addClass("disabled");
        $(".content-actions-game .button-player-ace.player2").addClass("disabled");
        $(".content-actions-game .button-player-fault.player2").addClass("disabled");
    }

    $(".content-actions-game .button-player.player1").removeClass("disabled");
    $(".content-actions-game .button-player.player2").removeClass("disabled");
    
}

function disableBottons(){
    $(".button-finish").addClass("disabled");
    $(".button-suspend").addClass("disabled");

    $(".content-actions-game .button-player.player1").addClass("disabled");
    $(".content-actions-game .button-player-ace.player1").addClass("disabled");
    $(".content-actions-game .button-player-fault.player1").addClass("disabled");

    $(".content-actions-game .button-player.player2").addClass("disabled");
    $(".content-actions-game .button-player-ace.player2").addClass("disabled");
    $(".content-actions-game .button-player-fault.player2").addClass("disabled");
    $(".content-point-game .button-return").addClass("disabled");
}

function calculatePoints(){
    var pointsp1 = 0;
    var pointsp2 = 0;
    
    var s1p1 = parseInt($(".content-results .player1 .s1 .value").html());
    var s1p2 = parseInt($(".content-results .player2 .s1 .value").html());

    if(actualSet > 1){
        if(s1p1>s1p2 && s1p1>=6) pointsp1++;
        if(s1p2>s1p1 && s1p2>=6) pointsp2++;
    }

    var s2p1 = parseInt($(".content-results .player1 .s2 .value").html());
    var s2p2 = parseInt($(".content-results .player2 .s2 .value").html());

    if(actualSet > 2){    
        if(s2p1>s2p2 && s2p1>=6) pointsp1++;
        if(s2p2>s2p1 && s2p2>=6) pointsp2++;
    }

    var s3p1 = parseInt($(".content-results .player1 .s3 .value").html());
    var s3p2 = parseInt($(".content-results .player2 .s3 .value").html());
    
    if(finish){
        if(s3p1>s3p2 && s3p1>=6) pointsp1++;
        if(s3p2>s3p1 && s3p2>=6) pointsp2++;
    }

    // $(".content-results .player1 .point .point").html(pointsp1);
    // $(".content-results .player2 .point .point").html(pointsp2); 

    $winner = "";

    if(pointsp1 > pointsp2){
        if(Math.abs(pointsp1-pointsp2) == 2){
            $("#modal-results-finish .winner.player1").removeClass("d-none");
            $winner = 1;
        }else{
            if(finish){
                $("#modal-results-finish .winner.player1").removeClass("d-none");
                $winner = 1;
            }
        }
    }else{
        if(Math.abs(pointsp1-pointsp2) == 2){
            $("#modal-results-finish .winner.player2").removeClass("d-none");
            $winner = 2;
        }else{
            if(finish){
                $("#modal-results-finish .winner.player2").removeClass("d-none");
                $winner = 2;
            }
        }
    }

    if($winner != ""){
        setValuesInResult();

        $("#modal-results-finish").modal("show");
        $(".button-play-pause .pause").trigger("click");

        finish = true;

        $.ajax({
            url: $("body").attr('data-baseurl')+"/match/winner",
            data: {
                "player": $winner,
            },
            type: 'POST'
        });
    }
}

function setValuesInResult(){
    $("#modal-results-finish .content-results-finish .player1 .s1 .value").html($(".content-results .player1 .s1 .value").html());
    $("#modal-results-finish .content-results-finish .player1 .s1 .tiebreak").html($(".content-results .player1 .s1 .tiebreak").html());
    $("#modal-results-finish .content-results-finish .player1 .s2 .value").html($(".content-results .player1 .s2 .value").html());
    $("#modal-results-finish .content-results-finish .player1 .s2 .tiebreak").html($(".content-results .player1 .s2 .tiebreak").html());
    $("#modal-results-finish .content-results-finish .player1 .s3 .value").html($(".content-results .player1 .s3 .value").html());
    $("#modal-results-finish .content-results-finish .player1 .s3 .tiebreak").html($(".content-results .player1 .s3 .tiebreak").html());

    $("#modal-results-finish .content-results-finish .player2 .s1 .value").html($(".content-results .player2 .s1 .value").html());
    $("#modal-results-finish .content-results-finish .player2 .s1 .tiebreak").html($(".content-results .player2 .s1 .tiebreak").html());
    $("#modal-results-finish .content-results-finish .player2 .s2 .value").html($(".content-results .player2 .s2 .value").html());
    $("#modal-results-finish .content-results-finish .player2 .s2 .tiebreak").html($(".content-results .player2 .s2 .tiebreak").html());
    $("#modal-results-finish .content-results-finish .player2 .s3 .value").html($(".content-results .player2 .s3 .value").html());
    $("#modal-results-finish .content-results-finish .player2 .s3 .tiebreak").html($(".content-results .player2 .s3 .tiebreak").html());
}

function resizeWindow(){
    if($(window).width() > 768){
        $(".content-results.vertical").addClass("d-none");
        $(".content-results.horizontal").removeClass("d-none");
    }else{
        $(".content-results.horizontal").addClass("d-none");
        $(".content-results.vertical").removeClass("d-none");
    }
}

function scoreSet(score_player1,score_player2, plays_user1, plays_user2, set){
    $.ajax({
        url: $("body").attr('data-baseurl')+"/match/scoreSet",
        data: {
            "score_player1": score_player1,
            "score_player2": score_player2,
            "set": set,
            "plays_user1": plays_user1,
            "plays_user2": plays_user2
        },
        type: 'POST'
    });
}

$(document).ready(function() {

    actualTime = getTime();
    
    resizeWindow();    

    if($(".content-time-match").attr("data-star") == 1){
        setTimeout(function(){
            $(".button-play-pause .play").trigger("click");
        }, 500);
    }

    if($(".content-actions-game .button-player.player1").hasClass("saca")){
        playerTake = 1;
        $(".content-actions-game .button-player.player1 .saca").addClass("active");
        $(".result-game-info .points-player-1 .saca-player-1").addClass("active");
        $(".content-results .player1 .ball").addClass("active");
    }

    if($(".content-actions-game .button-player.player2").hasClass("saca")){
        playerTake = 2;
        $(".content-actions-game .button-player.player2 .saca").addClass("active");
        $(".result-game-info .points-player-2 .saca-player-2").addClass("active");
        $(".content-results .player2 .ball").addClass("active");
    }

    actualSet = parseInt($(".content-results").attr("data-set"));
    playsUser1 = parseInt($(".content-results").attr("data-playsuseru"));
    playsUser2 = parseInt($(".content-results").attr("data-playsuserd"));

    if(parseInt($(".content-results .player1 .s"+actualSet+" .value").html()) == 6 && parseInt($(".content-results .player2 .s"+actualSet+" .value").html()) == 6){
        activeTieBreak = true;
    }

    // calculatePoints();

    $(document).on("click", ".button-play-pause .play", function(){
        if(!$(this).closest(".button-play-pause").hasClass("disabled")){
            if(!finish){
                if($(this).closest(".button-play-pause").attr("data-new") == 1){
                    $("#choose-who-starts").modal("show");
                }else{
                    startTime();
                    $(this).parent().find(".play").addClass("d-none");
                    $(this).parent().find(".pause").removeClass("d-none");

                    changeStatusButtonsMatch();

                    if(parseInt($(".points-player-1 .value").html()) == 0 && parseInt($(".points-player-2 .value").html()) == 0){
                        $(".button-return").addClass("disabled");
                    }else{
                        $(".button-return").removeClass("disabled");
                    }

                    if(playerTake == 1){
                        changeStatusPlayer1();
                    }else{
                        changeStatusPlayer2();
                    }
                }
            }else{
                $("#modal-results-finish").modal("show");
            }
        }
    });

    $(document).on("click", ".button-play-pause .pause", function(){
        if(!$(this).closest(".button-play-pause").hasClass("disabled")){
            $(this).parent().find(".play").removeClass("d-none");
            $(this).parent().find(".pause").addClass("d-none");
            stopTime();
            disableBottons();

            $.ajax({
                url: $("body").attr('data-baseurl')+"/match/scoreLog",
                data: {
                    "score_player1": $(".points-player-1 .value").html(),
                    "score_player2": $(".points-player-2 .value").html(),
                    "set": actualSet,
                    "plays_user1": playsUser1,
                    "plays_user2": playsUser2,
                    "type": 5
                },
                type: 'POST'
            });
        }
    });

    $(document).on("click", "#choose-who-starts .modal-content .choose-player", function(){
        $("#choose-who-starts").modal("hide");
        $(".button-play-pause").attr("data-new",0);
        startTime();
        $(".button-play-pause .play").addClass("d-none");
        $(".button-play-pause .pause").removeClass("d-none");

        changeStatusButtonsMatch();

        if($(this).hasClass("player1")){
            playerTake = 1;
            changeStatusPlayer1();
        }else{
            playerTake = 2;
            changeStatusPlayer2();
        }

        $.ajax({
            url: $("body").attr('data-baseurl')+"/match/scoreLog",
            data: {
                "player_take": playerTake,
                "type": 4
            },
            type: 'POST'
        });

        $.ajax({
            url: $("body").attr('data-baseurl')+"/match/start",
            data: {},
            type: 'POST'
        });
    });
    
    $(document).on("click", ".button-player.player1, .button-player.player2, .button-player-ace.player1, .button-player-ace.player2", function(){
        if(!$(this).hasClass("disabled")){
            var score_player1 = $(".points-player-1 .value").html();
            var score_player2 = $(".points-player-2 .value").html();
            
            if(!activeTieBreak){
                var index_player1 = valuesPoints.indexOf(score_player1);
                var index_player2 = valuesPoints.indexOf(score_player2);
            }

            console.log("activeTieBreak",activeTieBreak);
            console.log("score_player1",score_player1);
            console.log("score_player2",score_player2);
            console.log("index_player1",index_player1);
            console.log("index_player2",index_player2);

            var type = 0;
            var updateData = true;

            if($(this).hasClass("button-player-ace")){
                type = 1;
            }

            if($(this).hasClass("double")){
                $(this).removeClass("double");
                type = 3;
            }

            if($(this).hasClass("player1")){
                if(activeTieBreak){
                    score_player1 = parseInt(score_player1);
                    score_player2 = parseInt(score_player2);

                    console.log("score_player1",score_player1);
                    console.log("score_player2",score_player2);

                    if(score_player1 > score_player2 && score_player1 >= 6){
                        console.log("gana juego player1");
                        $("#validate-game .player-win-game").html(1);
                        $("#validate-game").modal("show");
                        updateData = false;
                    }else{
                        score_player1 = score_player1 + 1;

                        $(".content-results .player1 .s"+actualSet+" .tiebreak").html(score_player1);
                        
                        $(".points-player-1 .value").html(score_player1);

                        var numSaque = score_player1 + score_player2;
                        if(numSaque == 1 || (numSaque-1) % 2 == 0){
                            changeStatusPlayer1();
                            changeStatusPlayer2();
                            if(playerTake == 1){
                                playerTake = 2;
                            }else{
                                playerTake = 1;
                            }
                        }
                    }
                }else{
                    if(index_player1 == 5){
                        score_player1 = valuesPoints[3];
                        score_player2 = valuesPoints[3];
                        $(".points-player-1 .value").html(valuesPoints[3]);
                        $(".points-player-2 .value").html(valuesPoints[3]);
                        $(".content-results .player1 .point .point").html(valuesPoints[3]);
                        $(".content-results .player2 .point .point").html(valuesPoints[3]);
                    }else{
                        if(index_player1 == 4){
                            console.log("gana juego player1");
                            $("#validate-game .player-win-game").html(1);
                            $("#validate-game").modal("show");
                            updateData = false;
                        }else{
                            if(index_player1<3){
                                score_player1 = valuesPoints[index_player1+1];
                                $(".points-player-1 .value").html(valuesPoints[index_player1+1]);
                                $(".content-results .player1 .point .point").html(valuesPoints[index_player1+1]);
                            }else{
                                if(index_player1 == index_player2){
                                    score_player1 = valuesPoints[index_player1+1];
                                    score_player2 = valuesPoints[5];
                                    $(".points-player-1 .value").html(valuesPoints[index_player1+1]);
                                    $(".points-player-2 .value").html(valuesPoints[5]);
                                    $(".content-results .player1 .point .point").html(valuesPoints[index_player1+1]);
                                    $(".content-results .player2 .point .point").html(valuesPoints[5]);
                                }else{
                                    console.log("gana juego player1");
                                    $("#validate-game .player-win-game").html(1);
                                    $("#validate-game").modal("show");
                                    updateData = false;
                                }
                            }
                        }
                    }
                }
            }else{
                if(activeTieBreak){
                    score_player1 = parseInt(score_player1);
                    score_player2 = parseInt(score_player2);
                    
                    console.log("score_player1",score_player1);
                    console.log("score_player2",score_player2);

                    if(score_player2 > score_player1 && score_player2 >= 6){
                        console.log("gana juego player2");
                        $("#validate-game .player-win-game").html(2);
                        $("#validate-game").modal("show");
                        updateData = false;
                    }else{
                        score_player2 = score_player2 + 1;

                        $(".content-results .player2 .s"+actualSet+" .tiebreak").html(score_player2);

                        $(".points-player-2 .value").html(score_player2);

                        var numSaque = score_player1 + score_player2;
                        if(numSaque == 1 || (numSaque-1) % 2 == 0){
                            changeStatusPlayer1();
                            changeStatusPlayer2();
                            if(playerTake == 1){
                                playerTake = 2;
                            }else{
                                playerTake = 1;
                            }
                        }
                    }
                }else{
                    if(index_player2 == 5){
                        score_player1 = valuesPoints[3];
                        score_player2 = valuesPoints[3];
                        $(".points-player-1 .value").html(valuesPoints[3]);
                        $(".points-player-2 .value").html(valuesPoints[3]);
                        $(".content-results .player1 .point .point").html(valuesPoints[3]);
                        $(".content-results .player2 .point .point").html(valuesPoints[3]);
                    }else{
                        if(index_player2 == 4){
                            console.log("gana juego player2");
                            $("#validate-game .player-win-game").html(2);
                            $("#validate-game").modal("show");
                            updateData = false;
                        }else{
                            if(index_player2<3){
                                score_player2 = valuesPoints[index_player2+1];
                                $(".points-player-2 .value").html(valuesPoints[index_player2+1]);
                                $(".content-results .player2 .point .point").html(valuesPoints[index_player2+1]);
                            }else{
                                if(index_player1 == index_player2){
                                    score_player2 = valuesPoints[index_player1+1];
                                    score_player2 = valuesPoints[5];
                                    $(".points-player-2 .value").html(valuesPoints[index_player1+1]);
                                    $(".points-player-1 .value").html(valuesPoints[5]);
                                    $(".content-results .player1 .point .point").html(valuesPoints[5]);
                                    $(".content-results .player2 .point .point").html(valuesPoints[index_player1+1]);
                                }else{
                                    console.log("gana juego player2");
                                    $("#validate-game .player-win-game").html(2);
                                    $("#validate-game").modal("show");
                                    updateData = false;
                                }
                            }
                        }
                    }
                }
            }

            if(score_player2 == 0 && score_player1 == 0){
                $(".button-return").addClass("disabled");
            }else{
                $(".button-return").removeClass("disabled");
            }

            if(updateData){
                $.ajax({
                    url: $("body").attr('data-baseurl')+"/match/scoreLog",
                    data: {
                        "score_player1": score_player1,
                        "score_player2": score_player2,
                        "set": actualSet,
                        "plays_user1": playsUser1,
                        "plays_user2": playsUser2,
                        "player_take": playerTake,
                        "type": type
                    },
                    type: 'POST'
                });
            }
        }
    });
    
    $(document).on("click", ".button-player-fault.player1, .button-player-fault.player2", function(){
        if(!$(this).hasClass("disabled")){
            if($(this).hasClass("double")){
                $(this).parent().find(".button-player-fault").removeClass("d-none");
                $(this).parent().find(".button-player-fault.double").addClass("d-none");
                
                if($(this).hasClass("player1")){
                    $(".button-player.player2").addClass("double");
                    $(".button-player.player2").trigger("click");
                }else{
                    $(".button-player.player1").addClass("double");
                    $(".button-player.player1").trigger("click");
                }

            }else{
                $(this).addClass("d-none");
                $(this).parent().find(".button-player-fault.double").removeClass("d-none");

                $.ajax({
                    url: $("body").attr('data-baseurl')+"/match/scoreLog",
                    data: {
                        "score_player1": $(".points-player-1 .value").html(),
                        "score_player2": $(".points-player-2 .value").html(),
                        "set": actualSet,
                        "plays_user1": playsUser1,
                        "plays_user2": playsUser2,
                        "type": 2
                    },
                    type: 'POST'
                });
            }
        }
    });

    $(document).on("click", ".acept-game-win", function(){
        var score_player1 = parseInt($(".points-player-1 .value").html());
        var score_player2 = parseInt($(".points-player-2 .value").html());
        if(!activeTieBreak){
            var index_player1 = valuesPoints.indexOf(score_player1);
            var index_player2 = valuesPoints.indexOf(score_player2);
        }

        console.log("#validate-game .player-win-game",$("#validate-game .player-win-game").html());
        console.log("actualSet prev",actualSet);
        console.log("playsUser1",playsUser1);
        console.log("playsUser2",playsUser2);

        if($("#validate-game .player-win-game").html() == 1){
            if(playsUser1 >= 5 && playsUser2 < 5){
                
                scoreSet(0, 0, playsUser1 + 1, playsUser2, actualSet);

                $.ajax({
                    url: $("body").attr('data-baseurl')+"/match/scoreLog",
                    data: {
                        "score_player1": score_player1,
                        "score_player2": score_player2,
                        "set": actualSet,
                        "plays_user1": playsUser1 + 1,
                        "plays_user2": playsUser2,
                        "player_take": playerTake,
                        "type": 0
                    },
                    type: 'POST'
                });

                $(".content-results .player1 .s"+actualSet+" .value").html(playsUser1 + 1);

                if(actualSet <= 2){
                    actualSet = parseInt(actualSet) + 1;
                }else{
                    finish = true;
                } 

                playsUser1 = 0;
                playsUser2 = 0;        
            }else{
                if(playsUser1 >= 6 && playsUser2 == 6){
                    console.log("tt playsUser1",playsUser1);
                    $(".content-results .player1 .s"+actualSet+" .value").html(playsUser1 + 1);
                    console.log("tt playsUser1",playsUser1 + 1);
                    if(activeTieBreak){
                        console.log("score_player1",score_player1);
                        score_player1 = score_player1 + 1;
                        console.log("score_player1",score_player1);
                        $(".content-results .player1 .s"+actualSet+" .tiebreak").html(score_player1);

                        scoreSet(score_player1, score_player2, playsUser1 + 1, playsUser2, actualSet);

                    }else{
                        score_player1 = valuesPoints[index_player1+1];
                        score_player2 = valuesPoints[index_player2];
                    }

                    $.ajax({
                        url: $("body").attr('data-baseurl')+"/match/scoreLog",
                        data: {
                            "score_player1": score_player1,
                            "score_player2": score_player2,
                            "set": actualSet,
                            "plays_user1": playsUser1 + 1,
                            "plays_user2": playsUser2,
                            "player_take": playerTake,
                            "type": 0
                        },
                        type: 'POST'
                    });

                    if(actualSet < 3){
                        actualSet = parseInt(actualSet) + 1;
                    }else{
                        finish = true;
                    }  
                    playsUser1 = 0;
                    playsUser2 = 0;                
                }else{
                    scoreSet(0, 0, playsUser1 + 1, playsUser2, actualSet);
                    playsUser1++;
                }
            }

            if(!activeTieBreak) $(".content-results .player1 .s"+actualSet+" .value").html(playsUser1);
            $(".points-player-1 .value").html(valuesPoints[0]);
            $(".points-player-2 .value").html(valuesPoints[0]);
            $(".content-results .player1 .point .point").html(valuesPoints[0]);
            $(".content-results .player2 .point .point").html(valuesPoints[0]);
        }else{
            if(playsUser2 == 5 && playsUser1 < 5){

                scoreSet(0, 0, playsUser1, playsUser2 + 1, actualSet);

                $.ajax({
                    url: $("body").attr('data-baseurl')+"/match/scoreLog",
                    data: {
                        "score_player1": score_player1,
                        "score_player2": score_player2,
                        "set": actualSet,
                        "plays_user1": playsUser1,
                        "plays_user2": playsUser2 + 1,
                        "player_take": playerTake,
                        "type": 0
                    },
                    type: 'POST'
                });
                $(".content-results .player2 .s"+actualSet+" .value").html(playsUser2 + 1);

                if(actualSet <= 2){
                    actualSet = parseInt(actualSet) + 1;
                }else{
                    finish = true;
                }    
                playsUser1 = 0;
                playsUser2 = 0;             
            }else{
                if(playsUser1 == 6 && playsUser2 == 6){
                    $(".content-results .player2 .s"+actualSet+" .value").html(playsUser2 + 1);

                    if(activeTieBreak){
                        score_player2 = score_player2 + 1;
                        $(".content-results .player2 .s"+actualSet+" .tiebreak").html(score_player2);

                        scoreSet(score_player1, score_player2, playsUser1, playsUser2 + 1, actualSet);

                    }else{
                        score_player1 = valuesPoints[index_player1];
                        score_player2 = valuesPoints[index_player2+1];
                    }

                    $.ajax({
                        url: $("body").attr('data-baseurl')+"/match/scoreLog",
                        data: {
                            "score_player1": score_player1,
                            "score_player2": score_player2,
                            "set": actualSet,
                            "plays_user1": playsUser1,
                            "plays_user2": playsUser2 + 1,
                            "player_take": playerTake,
                            "type": 0
                        },
                        type: 'POST'
                    });

                    if(actualSet < 3){
                        actualSet = parseInt(actualSet) + 1;
                    }else{
                        finish = true;
                    } 
                    playsUser1 = 0;
                    playsUser2 = 0;                    
                }else{
                    scoreSet(0, 0, playsUser1, playsUser2 + 1, actualSet);
                    playsUser2++;
                }
            }

            if(!activeTieBreak) $(".content-results .player2 .s"+actualSet+" .value").html(playsUser2);

            $(".points-player-1 .value").html(valuesPoints[0]);
            $(".points-player-2 .value").html(valuesPoints[0]);
            $(".content-results .player1 .point .point").html(valuesPoints[0]);
            $(".content-results .player2 .point .point").html(valuesPoints[0]);
        }

        console.log("actualSet",actualSet);

        if(playerTake == 1){
            playerTake = 2;
        }else{
            playerTake = 1;
        }

        activeTieBreak = false;

        if(playsUser1 == 6 && playsUser2 == 6){
            activeTieBreak = true;
        }
        
        $("#validate-game").modal("hide");

        changeStatusPlayer1();
        changeStatusPlayer2();

        calculatePoints();

        if(parseInt($(".points-player-1 .value").html()) == 0 && parseInt($(".points-player-2 .value").html()) == 0){
            $(".button-return").addClass("disabled");
        }else{
            $(".button-return").removeClass("disabled");
        }

        if(!activeTieBreak){
            $.ajax({
                url: $("body").attr('data-baseurl')+"/match/scoreLog",
                data: {
                    "score_player1": $(".points-player-1 .value").html(),
                    "score_player2": $(".points-player-2 .value").html(),
                    "set": actualSet,
                    "plays_user1": playsUser1,
                    "plays_user2": playsUser2,
                    "player_take": playerTake,
                    "type": 0
                },
                type: 'POST'
            });
        }
    });

    $(document).on("click", ".cancel-game-win", function(){
        $("#validate-game").modal("hide");
    });

    $(document).on("click", ".button-suspend", function(){
        if(!$(this).hasClass("disabled")){
            $(".button-play-pause .pause").trigger("click");
        }
    });

    $(document).on("click", ".button-finish", function(){
        if(!$(this).hasClass("disabled")){
            $("#choose-retired-player").modal("show");
        }
    });

    $(document).on("click", "#choose-retired-player .modal-content .choose-player", function(){

        $("#choose-retired-player").modal("hide");

        $(".button-play-pause .pause").trigger("click");

        var retired = 2;
        if($("#validate-game .player-win-game").html() == 1){
            retired = 1;
            $("#modal-results-finish .winner.player2").removeClass("d-none");
        }else{
            $("#modal-results-finish .winner.player1").removeClass("d-none");
        }

        $.ajax({
            url: $("body").attr('data-baseurl')+"/match/retired",
            data: {
                "player": retired,
            },
            type: 'POST'
        });

        finish = true;

        setValuesInResult();
        
        $("#modal-results-finish").modal("show");
        
    });

    $(document).on("click", ".button-return", function(){
        if(!$(this).hasClass("disabled")){
            $.ajax({
                url: $("body").attr('data-baseurl')+"/match/prevLog",
                data: {},
                type: 'POST',
                dataType: "json",
                success: function(data){
                    data = data.data;
                    if(data.length > 0){
                        data = data[0];

                        $(".points-player-1 .value").html(data.score_user1);
                        $(".points-player-2 .value").html(data.score_user2);
                        $(".content-results .player1 .point .point").html(data.score_user1);
                        $(".content-results .player2 .point .point").html(data.score_user2);

                        if(data.score_user1 == 0 && data.score_user2 == 0){
                            $(".button-return").addClass("disabled");
                        }else{
                            $(".button-return").removeClass("disabled");
                        }
                    }
                }
            });
        }
    });    
});

$(window).resize(function() {
    resizeWindow();
});   