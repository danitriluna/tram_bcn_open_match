<?php 

namespace App\Service;

class MatchService
{
    public function getFormatDate($date, $lang = "en"): string
    {      
        if($lang == "en"){
            $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        }else{
            $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        }
        
        $month = $months[intval(date("m", strtotime($date))) - 1];
        return date("d", strtotime($date)) . " " . $month . " - " . date("H:i", strtotime($date));    
    }

    public function getRound($round, $lang = "en"): string
    {
        if($lang == "en"){
            switch($round->key){
                case "1":
                    return "1ST ROUND";
                    break;
                case "2":
                    return "2ND ROUND";
                    break;
                case "3":
                    return "3RD ROUND";
                    break;
            }
        }else{
            switch($round->key){
                case "1":
                    return "1ª RONDA";
                    break;
                case "2":
                    return "2ª RONDA";
                    break;
                case "3":
                    return "3ª RONDA";
                    break;
            }
        }
    }

    public function getCourt($court, $id_lang): string
    {
        return $court->lang_info[$id_lang]->name;
    }
}

?>