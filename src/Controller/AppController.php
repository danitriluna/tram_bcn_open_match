<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class AppController extends AbstractController
{

    /**
     * @Route("/", name="app_home", defaults={"_locale":"%locale%"}, methods={"GET"})
     * @Route("/{_locale}/", requirements={"_locale":"%app_locales%"}, methods={"GET"})
     */
    public function index(): Response
    {
        // return $this->render('home/index.html.twig', [
        //     'controller_name' => 'HomeController',
        // ]);
        return $this->redirectToRoute('app_login');
    }

    /**
     * @Route("/login", name="app_login", defaults={"_locale":"%locale%"}, methods={"GET", "POST"})
     * @Route("/{_locale}/login", requirements={"_locale":"%app_locales%"}, methods={"GET", "POST"})
     */
    public function login( SessionInterface $session, HttpClientInterface $httpClient, Request $request): Response
    {

        $form_error = "";
        $last_name = "";

        if ( ($request->request->get('submit') !== null) && ($request->request->get('submit') != "") ) {

            if ( ($request->request->get('username') === null) || ($request->request->get('username') == "")) {
                $form_error = "Username needed";
            } else {
                $last_name = $request->request->get('username');
                if ( ($request->request->get('password') === null) || ($request->request->get('password') == "")) {
                    $form_error = "Password needed";
                } else {
                    $response = $httpClient->request(
                        'POST',
                        $this->getParameter('API_base_url') . '/oauth/v2/token',
                        [
                            'body' => [
                                "client_id" => $this->getParameter('API_client_id'),
                                "client_secret" => $this->getParameter('API_client_secret'),
                                "grant_type" => "password",
                                "username" => $request->request->get('username'),
                                "password" => $request->request->get('password'),
                                "lang" => "es"
                            ]
                        ]
                    );

                    $decodedPayload = (array) json_decode($response->getContent(false));

                    //localStorage.setItem(environment.authTokenKey, res.access_token);


                    if ( array_key_exists("error", $decodedPayload) ) {
                        $session->set('user-session', null);
                        $session->set('user-data', null);
                    } else {
                        $session->set('user-session', $decodedPayload);

                        $response = $httpClient->request(
                            'GET',
                            $this->getParameter('API_base_url') . '/api/user/token/' . $decodedPayload['access_token'],
                            [
                                'headers' => [
                                    'Authorization' => 'Bearer ' . $decodedPayload['access_token'],
                                ]
                            ]
                        );
                        $decodedPayload = $response->toArray();

                        /* De moment deixo aquí l'estructura de dades que s'obté, per a tenir una guia de cara a utilitza-ho
                        Array
                        (
                            [id] => 1
                            [username] => testuser
                            [roles] => Array
                                (
                                    [0] => 1
                                )
                        
                            [roleNames] => Array
                                (
                                    [0] => ROLE_ADMIN
                                    [1] => ROLE_USER
                                )
                        
                            [name] => test
                        )
                        */

                        // echo '<pre>';var_dump($decodedPayload); die();

                        /* De moment poso també el (1) ROLE_ADMIN per a veure que passa */
                        if ( !in_array(1, $decodedPayload['roles']) && !in_array(5, $decodedPayload['roles']) && !in_array("ROLE_ARBITRATOR", $decodedPayload['roleNames']) ) {
                            $form_error = "Not authorized user";
                        } else {
                            $session->set('user-data', $decodedPayload);

                            return $this->redirectToRoute('match_home');
                        }                        
                    }

                }
            }
    
        }
        
        return $this->render('security/login.html.twig', [
            'controller_name' => 'HomeController',
            'error' => $form_error,
            'last_username' => $last_name
        ]);
    }

    /**
     * @Route("/logout", name="app_logout", defaults={"_locale":"%locale%"}, methods={"GET", "POST"})
     * @Route("/{_locale}/logout", requirements={"_locale":"%app_locales%"}, methods={"GET", "POST"})
     */
    public function logout( SessionInterface $session, Request $request): Response
    {
        $session->set('user-session', null);
        $session->set('user-data', null);

        return $this->redirectToRoute('app_login');
    }
}
