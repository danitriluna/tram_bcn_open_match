<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use App\Service\MatchService;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class MatchController extends AbstractController
{

    /**
     * @Route("/match", name="match_home", defaults={"_locale":"%locale%"}, methods={"GET"})
     * @Route("/{_locale}/match", requirements={"_locale":"%app_locales%"}, methods={"GET"})
     */
    public function index(MatchService $matchService, SessionInterface $session, HttpClientInterface $httpClient, Request $request): Response
    {
        $user = $session->get("user-data");
        $token = $session->get("user-session");

        $response = $httpClient->request(
            'GET',
            $this->getParameter('API_base_url') . '/api/matches/arbitro/' . $user["id"],
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token['access_token'],
                ]
            ]
        );

        $decodedPayload = (array) json_decode($response->getContent(false));

        // echo '<pre>'; var_dump($decodedPayload); die();

        if(isset($decodedPayload["error"])){
            if($decodedPayload["error"] == "invalid_grant"){
                return $this->redirectToRoute('app_login');
            }
        }

        if(count($decodedPayload) > 0){

            $decodedPayload = $decodedPayload[0];

            // echo '<pre>'; var_dump($decodedPayload); die();

            $session->set('match_id', $decodedPayload->id);
            $session->set('player1_id', $decodedPayload->player1->id);
            $session->set('player2_id', $decodedPayload->player2->id);

            $duration = $decodedPayload->duration;
            // if($session->get('duration') > $decodedPayload->duration){
            //     $duration = $session->get('duration');
            // }

            $diffHours = (strtotime($decodedPayload->date) - strtotime(date('Y-m-d H:i:s')))/(60*60);
            $maxDiffHours = 1;

            $active_match = ($diffHours <= $maxDiffHours)? true : false;

            $response = $httpClient->request(
                'GET',
                $this->getParameter('API_base_url') . '/api/scoreLog/' . $decodedPayload->id,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token['access_token'],
                    ]
                ]
            );

            $socreLog = (array) json_decode($response->getContent(false));

            $response = $httpClient->request(
                'GET',
                $this->getParameter('API_base_url') . '/api/scoreLog/sets/' . $decodedPayload->id,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token['access_token'],
                    ]
                ]
            );

            $sets = (array) json_decode($response->getContent(false));

            $listSets = array();
            $listSets["player1"] = array();
            $listSets["player1"][1] = 0;
            $listSets["player1"]["tiebreak"][1] = "";
            $listSets["player1"][2] = 0;
            $listSets["player1"]["tiebreak"][2] = "";
            $listSets["player1"][3] = 0;
            $listSets["player1"]["tiebreak"][3] = "";
            $listSets["player2"] = array();
            $listSets["player2"][1] = 0;
            $listSets["player2"]["tiebreak"][1] = "";
            $listSets["player2"][2] = 0;
            $listSets["player2"]["tiebreak"][2] = "";
            $listSets["player2"][3] = 0;
            $listSets["player2"]["tiebreak"][3] = "";

            if($sets){
                foreach($sets as $set){
                    $listSets["player1"][$set->set] = $set->pu1;
                    $listSets["player2"][$set->set] = $set->pu2;
                    $listSets["player1"]["tiebreak"][$set->set] = ($set->tiebreak[0]->scoreUser1 > 0 && $set->tiebreak[0]->scoreUser1 < 15)? $set->tiebreak[0]->scoreUser1 : "" ;
                    $listSets["player2"]["tiebreak"][$set->set] = ($set->tiebreak[0]->scoreUser2 > 0 && $set->tiebreak[0]->scoreUser2 < 15)? $set->tiebreak[0]->scoreUser2 : "" ;
                }
            }

            $timeStarted = 0;
            $isNew = 1;
            $playerTake = false;
            $score_player1 = 0;
            $score_player2 = 0;
            $actualSet = 1;
            $plays_user1 = 0;
            $plays_user2 = 0;
            $type = 0;

            if(count($socreLog) > 0){
                $socreLog = $socreLog[0];
                $playerTake = $socreLog->user_take->id;
                $session->set("player_take", $socreLog->user_take->id);
                $isNew = 0;
                $score_player1 = $socreLog->score_user1;
                $score_player2 = $socreLog->score_user2;
                $actualSet = $socreLog->set;
                $plays_user1 = $socreLog->plays_user1;
                $plays_user2 = $socreLog->plays_user2;
                $type = $socreLog->type;
                if($socreLog->type != 5) $timeStarted = 1;
            }

            $namePlayer1 = $decodedPayload->player1->player1->name." ".$decodedPayload->player1->player1->last_name1;
            $namePlayer2 = $decodedPayload->player2->player1->name." ".$decodedPayload->player2->player1->last_name1;

            $duo = false;

            if(property_exists($decodedPayload->player1, "player2")){
                $duo = true;
                $namePlayer1 = $decodedPayload->player1->player1->last_name1." - ".$decodedPayload->player1->player2->last_name1;
            }
            if(property_exists($decodedPayload->player2, "player2")){
                $duo = true;
                $namePlayer2 = $decodedPayload->player2->player1->last_name1." - ".$decodedPayload->player2->player2->last_name1;
            }

            return $this->render('dashboard/index.html.twig', [
                'controller_name' => 'DashboardController',
                'active_match' => $active_match,
                'match' => $decodedPayload,
                'date_match' => $matchService->getFormatDate($decodedPayload->date, "es"),
                'round' => $matchService->getRound($decodedPayload->round, "es"),
                'court' => $matchService->getCourt($decodedPayload->court, 2),
                'is_new' => $isNew,
                'time_started' => $timeStarted,
                'duration' => gmdate("H:i", $duration),
                'player_take' => $playerTake,
                'score_player1' => $score_player1,
                'score_player2' => $score_player2,
                'plays_user1' => $plays_user1,
                'plays_user2' => $plays_user2,
                'actualSet' => $actualSet,
                'type' => $type,
                'listSets'=> $listSets,
                'namePlayer1' => $namePlayer1,
                'namePlayer2' => $namePlayer2,
                'duo' => $duo,
                'user_session' => $session->get('user-session'),
                'user_data' => $session->get('user-data')
            ]);
        }else{
            return $this->render('dashboard/no-results.html.twig', [
                'controller_name' => 'DashboardController',
                'user_session' => $session->get('user-session'),
                'user_data' => $session->get('user-data')
            ]);
        }
    }

    /**
     * @Route("/match/updateTime", name="match_update_time", defaults={"_locale":"%locale%"}, methods={"POST"})
     * @Route("/{_locale}/match/updateTime", requirements={"_locale":"%app_locales%"}, methods={"POST"})
     */
    public function updateTime(MatchService $matchService, SessionInterface $session, HttpClientInterface $httpClient, Request $request): Response
    {
        $token = $session->get("user-session");

        $session->set('duration', $request->get("time"));

        $response = $httpClient->request(
            'POST',
            $this->getParameter('API_base_url') . '/api/matches/updateTime',
            [
                'body' => [
                    "match_id" => $session->get("match_id"),
                    "time" => $request->get("time")
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $token['access_token'],
                ]
            ]
        );

        return new Response();
    }

    /**
     * @Route("/match/scoreLog", name="match_scoreLog", defaults={"_locale":"%locale%"}, methods={"POST"})
     * @Route("/{_locale}/match/scoreLog", requirements={"_locale":"%app_locales%"}, methods={"POST"})
     */
    public function addScore(MatchService $matchService, SessionInterface $session, HttpClientInterface $httpClient, Request $request): Response
    {
        $token = $session->get("user-session");

        if($request->get("player_take")){
            $player_take = $request->get("player_take");
            if($player_take == 1){
                $player_take = $session->get('player1_id');
            }else{
                $player_take = $session->get('player2_id');
            }
            $session->set("player_take", $player_take);
        }else{
            $player_take = $session->get("player_take");
        }

        $score_player1 = 0;
        if($request->get("score_player1")){
            $score_player1 = $request->get("score_player1");
        }

        $score_player2 = 0;
        if($request->get("score_player2")){
            $score_player2 = $request->get("score_player2");
        }

        $set = 1;
        if($request->get("set")){
            $set = $request->get("set");
        }

        $type = 0;
        if($request->get("type")){
            $type = $request->get("type");
        }

        $plays_user1 = 0;
        if($request->get("plays_user1")){
            $plays_user1 = $request->get("plays_user1");
        }

        $plays_user2 = 0;
        if($request->get("plays_user2")){
            $plays_user2 = $request->get("plays_user2");
        }

        $response = $httpClient->request(
            'PUT',
            $this->getParameter('API_base_url') . '/api/scoreLog/add',
            [
                'body' => [
                    "match_id" => $session->get("match_id"),
                    "player_take" => $player_take,
                    "score_player1" => $score_player1,
                    "score_player2" => $score_player2,
                    "set" => $set,
                    "type" => $type,
                    "plays_user1" => $plays_user1,
                    "plays_user2" => $plays_user2
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $token['access_token'],
                ]
            ]
        );

        return new Response();
    }

    /**
     * @Route("/match/start", name="match_start", defaults={"_locale":"%locale%"}, methods={"POST"})
     * @Route("/{_locale}/match/start", requirements={"_locale":"%app_locales%"}, methods={"POST"})
     */
    public function startMatch(MatchService $matchService, SessionInterface $session, HttpClientInterface $httpClient, Request $request): Response
    {
        $token = $session->get("user-session");

        $response = $httpClient->request(
            'POST',
            $this->getParameter('API_base_url') . '/api/matches/start',
            [
                'body' => [
                    "match_id" => $session->get("match_id")
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $token['access_token'],
                ]
            ]
        );

        return new Response();
    }

    /**
     * @Route("/match/prevLog", name="match_prev_log", defaults={"_locale":"%locale%"}, methods={"POST"})
     * @Route("/{_locale}/match/prevLog", requirements={"_locale":"%app_locales%"}, methods={"POST"})
     */
    public function prevLog(MatchService $matchService, SessionInterface $session, HttpClientInterface $httpClient, Request $request): Response
    {
        $token = $session->get("user-session");

        $response = $httpClient->request(
            'POST',
            $this->getParameter('API_base_url') . '/api/scoreLog/prev',
            [
                'body' => [
                    "match_id" => $session->get("match_id")
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $token['access_token'],
                ]
            ]
        );

        $log = json_decode($response->getContent(false));

        $jsonArray = array(
            'status' => 'success',
            'data' => $log,
        );
        
        $response = new Response(json_encode($jsonArray));
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');
        return $response;
    }

    /**
     * @Route("/match/retired", name="match_retired", defaults={"_locale":"%locale%"}, methods={"POST"})
     * @Route("/{_locale}/match/retired", requirements={"_locale":"%app_locales%"}, methods={"POST"})
     */
    public function retiredMatch(MatchService $matchService, SessionInterface $session, HttpClientInterface $httpClient, Request $request): Response
    {
        $token = $session->get("user-session");

        if($request->get("player")){
            if($request->get("player") == 1){
                $player_id = $session->get('player1_id');
                $winner_id = $session->get('player2_id');
            }else{
                $player_id = $session->get('player2_id');
                $winner_id = $session->get('player1_id');
            }
        }

        $response = $httpClient->request(
            'POST',
            $this->getParameter('API_base_url') . '/api/matches/retired',
            [
                'body' => [
                    "match_id" => $session->get("match_id"),
                    "player_id" => $player_id,
                    "winner_id" => $winner_id
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $token['access_token'],
                ]
            ]
        );

        return new Response();
    }

    /**
     * @Route("/match/winner", name="match_winner", defaults={"_locale":"%locale%"}, methods={"POST"})
     * @Route("/{_locale}/match/winner", requirements={"_locale":"%app_locales%"}, methods={"POST"})
     */
    public function winnerMatch(MatchService $matchService, SessionInterface $session, HttpClientInterface $httpClient, Request $request): Response
    {
        $token = $session->get("user-session");

        if($request->get("player")){
            if($request->get("player") == 1){
                $winner_id = $session->get('player2_id');
            }else{
                $winner_id = $session->get('player1_id');
            }
        }

        $response = $httpClient->request(
            'POST',
            $this->getParameter('API_base_url') . '/api/matches/winner',
            [
                'body' => [
                    "match_id" => $session->get("match_id"),
                    "winner_id" => $winner_id
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $token['access_token'],
                ]
            ]
        );

        return new Response();
    }

    /**
     * @Route("/match/scoreSet", name="match_scoreSet", defaults={"_locale":"%locale%"}, methods={"POST"})
     * @Route("/{_locale}/match/scoreSet", requirements={"_locale":"%app_locales%"}, methods={"POST"})
     */
    public function addScoreSet(MatchService $matchService, SessionInterface $session, HttpClientInterface $httpClient, Request $request): Response
    {
        $token = $session->get("user-session");

        $set = 1;
        if($request->get("set")){
            $set = $request->get("set");
        }

        $type = 0;
        if($request->get("type")){
            $type = $request->get("type");
        }

        $plays_user1 = 0;
        if($request->get("plays_user1")){
            $plays_user1 = $request->get("plays_user1");
        }

        $plays_user2 = 0;
        if($request->get("plays_user2")){
            $plays_user2 = $request->get("plays_user2");
        }

        $points_user1 = 0;
        if($request->get("score_player1")){
            $points_user1 = $request->get("score_player1");
        }

        $points_user2 = 0;
        if($request->get("score_player2")){
            $points_user2 = $request->get("score_player2");
        }

        $response = $httpClient->request(
            'PUT',
            $this->getParameter('API_base_url') . '/api/score/add',
            [
                'body' => [
                    "match_id" => $session->get("match_id"),
                    "points_user1" => $points_user1,
                    "points_user2" => $points_user2,
                    "set" => $set,
                    "plays_user1" => $plays_user1,
                    "plays_user2" => $plays_user2
                ],
                'headers' => [
                    'Authorization' => 'Bearer ' . $token['access_token'],
                ]
            ]
        );

        return new Response();
    }
    

}